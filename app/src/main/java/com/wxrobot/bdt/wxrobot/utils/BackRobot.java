package com.wxrobot.bdt.wxrobot.utils;

import android.content.Intent;

import com.wxrobot.bdt.wxrobot.MyApplication;

public class BackRobot {
    public static void backHome() {
        Intent mHomeIntent = new Intent(Intent.ACTION_MAIN);

        mHomeIntent.addCategory(Intent.CATEGORY_HOME);
        mHomeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
        MyApplication.getInstance().startActivity(mHomeIntent);

    }


}
