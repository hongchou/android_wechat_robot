package com.wxrobot.bdt.wxrobot.utils;

import android.accessibilityservice.AccessibilityService;
import android.util.Log;

import com.wxrobot.bdt.wxrobot.service.RobotService;

/**
 * 存储信息的工具类
 */
public class SevaInfoUtil {

    private static boolean isSaveNickName = false;
    private static boolean isSaveWxId = false;
    public static boolean isGetMyInfo = false;


    public static void getMyselfInfo(AccessibilityService accessibilityService) {
        /**
         * 获取客服微信信息
         * 从微信主页面才能获取微信信息
         */
//        进入我的界面
        WechatUtils.findTextAndClick(accessibilityService, "我");
        //如果在好友详情页面，先返回（到聊天页面）
        if (WechatUtils.findViewId(accessibilityService, RobotService.BaseLayoutId + "k5")) {
            WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "k5");
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//如果在微信聊天页面，先返回到微信主页面
        if (WechatUtils.findViewId(accessibilityService, RobotService.BaseLayoutId + "jv")) {
            WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "jv");
        }
//        String nickeName = WechatUtils.findTextById(accessibilityService, RobotService.BaseLayoutId + "a5b");
//        String wxId = WechatUtils.findTextById(accessibilityService, RobotService.BaseLayoutId + "d7y");
        String nickeName = WechatUtils.findTextById(accessibilityService, RobotService.BaseLayoutId + "a63");
        String wxId = WechatUtils.findTextById(accessibilityService, RobotService.BaseLayoutId + "dag");
        Log.e(RobotService.TAG, "微信昵称：" + nickeName);
        Log.e(RobotService.TAG, "微信号：" + wxId);

        if (nickeName != null) {
            SharedPreferencesUtil.mEditor.putString("微信昵称", nickeName);
            isSaveNickName = true;
        }

        if (wxId != null) {
            SharedPreferencesUtil.mEditor.putString("微信号", wxId);
            isSaveWxId = true;
        }
        if (isSaveWxId && isSaveNickName) {
            boolean isCommit = SharedPreferencesUtil.mEditor.commit();
            if (isCommit) {
                Log.e(RobotService.TAG, "客服号微信信息保存成功！");
                isGetMyInfo = true;
            }

        }
    }

}
