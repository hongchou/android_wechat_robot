package com.wxrobot.bdt.wxrobot.bean;

public class Data {
    private String device_id;

    private int status;

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Data{" +
                "device_id='" + device_id + '\'' +
                ", status=" + status +
                '}';
    }
}
