package com.wxrobot.bdt.wxrobot.utils;

import android.accessibilityservice.AccessibilityService;
import android.text.TextUtils;
import android.util.Log;

import com.wxrobot.bdt.wxrobot.net.NetUtil;
import com.wxrobot.bdt.wxrobot.service.RobotService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * 存储信息的工具类
 */
public class SaveInfoUtil {

    private static boolean isSaveNickName = false;
    private static boolean isSaveWxId = false;
    public static boolean isGetMyInfo = false;


    public static void getMyselfInfo(AccessibilityService accessibilityService) {
        /**
         * 获取客服微信信息
         * 从微信主页面才能获取微信信息
         */
//        进入我的界面
        WechatUtils.findTextAndClick(accessibilityService, "我");
        //如果在好友详情页面，先返回（到聊天页面）
        if (WechatUtils.findViewId(accessibilityService, RobotService.BaseLayoutId + "k5")) {
            WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "k5");
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//如果在微信聊天页面，先返回到微信主页面
        if (WechatUtils.findViewId(accessibilityService, RobotService.BaseLayoutId + "jv")) {
            WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "jv");
        }
//        String nickeName = WechatUtils.findTextById(accessibilityService, RobotService.BaseLayoutId + "a5b");
//        String wxId = WechatUtils.findTextById(accessibilityService, RobotService.BaseLayoutId + "d7y");
        String nickeName = WechatUtils.findTextById(accessibilityService, RobotService.BaseLayoutId + "a63");
        String wxId = WechatUtils.findTextById(accessibilityService, RobotService.BaseLayoutId + "dag");
        Log.e(RobotService.TAG, "微信昵称：" + nickeName);
        Log.e(RobotService.TAG, "微信号：" + wxId);

        if (nickeName != null) {
            SharedPreferencesUtil.mEditor.putString("微信昵称", nickeName);
            isSaveNickName = true;
        }

        if (wxId != null) {
            SharedPreferencesUtil.mEditor.putString("微信号", wxId);
            isSaveWxId = true;
//            获取mch_id
            NetUtil.getMch(wxId.substring(4));
        }
        if (isSaveWxId && isSaveNickName) {
            boolean isCommit = SharedPreferencesUtil.mEditor.commit();
            if (isCommit) {
                Log.e(RobotService.TAG, "客服号微信信息保存成功！");
                isGetMyInfo = true;
            }

        }
    }

    public static void updateWechet() {
        /**
         * 更新微信客服号添加好友微信信息（统计客服号添加好友的人数）
         */
        Log.e(RobotService.TAG, "更新微信客服号添加好友的信息");
//        客服微信号
        String mWx_id = SharedPreferencesUtil.sharedPreferences.getString("微信号", "");
        if (!TextUtils.isEmpty(mWx_id)) {
            /**
             * 判断客服微信信息是否为空
             */

            String wx_id = mWx_id.substring(4);
//            String mUrl = "http://192.168.1.122:8051/awxrobot/api/update/weixin/?wx_account=" + wx_id + "&today_added=true";
            String mUrl = "http://47.106.84.115:8051/awxrobot/api/update/weixin/?wx_account=" + wx_id + "&today_added=true";
            Log.e(RobotService.TAG, "更新好友微信信息mUrl:" + mUrl);
            try {
                URL url = new URL(mUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                //自由定制
                connection.setRequestMethod("GET");
                connection.setConnectTimeout(80000);
                connection.setReadTimeout(8000);
//            使用getInputStream获取服务器返回的输入流
                InputStream stresm = connection.getInputStream();
                //对返回的输入流进行读取
                BufferedReader reader = new BufferedReader(new InputStreamReader(stresm));
                StringBuilder response = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {

                    response.append(line);
                }
                Log.e(RobotService.TAG, "更新客服号添加好友信息返回值：" + response.toString());


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();

            }
        }


    }

}
