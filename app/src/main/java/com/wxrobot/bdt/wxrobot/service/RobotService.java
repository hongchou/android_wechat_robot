package com.wxrobot.bdt.wxrobot.service;

import android.accessibilityservice.AccessibilityService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

import com.wxrobot.bdt.wxrobot.MainActivity;
import com.wxrobot.bdt.wxrobot.net.NetUtil;
import com.wxrobot.bdt.wxrobot.utils.AutoAddFriendAcceptUtil;
import com.wxrobot.bdt.wxrobot.utils.MassMessageUtil;
import com.wxrobot.bdt.wxrobot.utils.SaveInfoUtil;
import com.wxrobot.bdt.wxrobot.utils.WechatUtils;

import static com.wxrobot.bdt.wxrobot.utils.SharedPreferencesUtil.editor;


public class RobotService extends AccessibilityService {

    public static final String TAG = "RobotService";
    //微信包名
    private final static String WeChat_PNAME = "com.tencent.mm";
    //微信布局ID前缀
    public static final String BaseLayoutId = "com.tencent.mm:id/";
    //微信首页
    public static final String WECHAT_CLASS_LAUNCHUI = "com.tencent.mm.ui.LauncherUI";
    //微信聊天页面
    public static final String WECHAT_CLASS_CHATUI = "com.tencent.mm.ui.chatting.ChattingUI";


    public static boolean isSendSuccess; //true 发送完成，  false 开始发送，还没发送

    public Notification notification = null;
    public String content = null;
    //    判断是否是从通知栏进入微信聊天界面
    public boolean fromNotification = false;
    //    被添加为好友
    public static boolean isAddFriend = false;
    //    消息推送一次标识
    public static boolean isSendOne = true;
    private String[] contents;
    private String contentLast;
    private String response;
    //    推送消息的内容
    private String msg;


    /***
     * id info:
     *      alm:聊天文本输入框
     * @param event
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {


        String className = event.getClassName().toString();
        Log.i(TAG, "event >> TYPE:" + event.getEventType());
        Log.i(TAG, "event >> ClassName:" + className);

        switch (event.getEventType()) {
            case AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED:
//                Log.e(TAG, "监听到通知栏变化。。。。。。。。。。。");
                fromFriendAddNotification(event);
                break;
            case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:

//                    自动添加好友，并推送消息
                if (isAddFriend) {
                    AutoAddFriendAcceptUtil.autoAddFriendAccept(this);
//推送消息给好友
//                    if (SaveInfoUtil.isSaveComplete && isSendOne) {
                    if (isSendOne) {
                        try {
                            WechatUtils.findTextAndClick(this, "发消息");


//        查找文本输入框，并粘贴内容

//                            if (!WechatUtils.findViewId(this, RobotService.BaseLayoutId + "alm")) {
                            if (!WechatUtils.findViewId(this, RobotService.BaseLayoutId + "amb")) {

                                Thread.sleep(500);
                                msg = "亲爱的书友您好，感谢您的充值！";
//                                msg = "亲爱的书友您好，感谢您的充值，点击此链接激活充值书币方可继续阅读:\n" + AutoAddFriendAcceptUtil.pushUrl;
//                                msg = "亲爱的每日看书书友您好，我是您的专属顾问丹丹，以后看书遇到什么问题可以找我哦，平台优惠活动我也会及时通知您的[爱心]" + "\n" + "您刚阅读的小说链接：" +
//                                        "您刚阅读的小说链接：" + AutoAddFriendAcceptUtil.pushUrl + "（也可进入公众号" + AutoAddFriendAcceptUtil.gzh_name + "继续阅读）";
//
//                                msg = "成功添加好友xxx";
                                WechatUtils.findViewByIdAndPasteContent(this, RobotService.BaseLayoutId + "amb", msg);

                            } else {
                                msg = "亲爱的书友您好，感谢您的充值！";
//                                msg = "亲爱的书友您好，感谢您的充值，点击此链接激活充值书币方可继续阅读:\n" + AutoAddFriendAcceptUtil.pushUrl;
//                                msg = "亲爱的每日看书书友您好，我是您的专属顾问丹丹，以后看书遇到什么问题可以找我哦，平台优惠活动我也会及时通知您的[爱心]" + "\n" + "您刚阅读的小说链接：" +
//                                        "您刚阅读的小说链接：" + AutoAddFriendAcceptUtil.pushUrl + "（也可进入公众号" + AutoAddFriendAcceptUtil.gzh_name + "继续阅读）";
//                                msg = "成功添加好友xxx";
                                WechatUtils.findViewByIdAndPasteContent(this, RobotService.BaseLayoutId + "amb", msg);
                            }


                            // 点击发送
//                        boolean send = WechatUtils.findViewId(this, RobotService.BaseLayoutId + "als");
                            if (WechatUtils.findViewId(this, RobotService.BaseLayoutId + "ami")) {
//                                发现粘贴内容后的发送按钮
                                WechatUtils.findTextAndClick(this, "发送");
//                                msg="";
//                                isSendOne = false;
                                isAddFriend = false;
//                                WechatUtils.findViewIdAndClick(this, RobotService.BaseLayoutId + "k5");
                                //                                推送微信公众号
//                                点击+  id:amh
                                WechatUtils.findViewIdAndClick(this, RobotService.BaseLayoutId + "amh");
//                                WechatUtils.findTextAndClick(this, "我的收藏");
                                WechatUtils.findTextAndClick(this, "我的收藏");
//                                点击收藏的内容ID：xg
//                                WechatUtils.findViewIdAndClick(this,RobotService.BaseLayoutId+"xg");
                                Thread.sleep(500);
                                WechatUtils.findTextAndClick(this, "万象书吧");


//                                点击发送
                                WechatUtils.findTextAndClick(this, "发送");
//返回到聊天页面
//                                WechatUtils.findViewIdAndClick(this, RobotService.BaseLayoutId + "kb");
//
//                                if (WechatUtils.findViewId(this, RobotService.BaseLayoutId + "k2")) {
//                                    WechatUtils.findViewIdAndClick(this, RobotService.BaseLayoutId + "k2");
//                                }else {
//                                    Thread.sleep(500);
//                                    WechatUtils.findViewIdAndClick(this, RobotService.BaseLayoutId + "k2");
//                                }
                                WechatUtils.findTextAndClick(this, "返回");
                                if (WechatUtils.findViewId(this, RobotService.BaseLayoutId + "jy")) {
//                                    判断是否回到了聊天界面
                                    Log.e(RobotService.TAG, "消息推送完成，回到了聊天界面");
//                                    WechatUtils.findTextAndClick(this, "返回");
                                    Thread.sleep(500);
                                    WechatUtils.findViewIdAndClick(this, RobotService.BaseLayoutId + "k2");
                                }

//                                更新客服号，好友添加人数
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        SaveInfoUtil.updateWechet();
                                    }
                                }).start();
//                                if (!isSendOne) {
//                                    isSendOne = true;
//
//                                    WechatUtils.findViewIdAndClick(this, RobotService.BaseLayoutId + "k5");
//                                返回手机主界面
//                                Thread.sleep(500);
//                                BackRobot.backHome();

//                                }

                            }


                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
//                    -------------------

                }
//                else {
////                    关键字回复
//                    if (content != null) {
//
//                        keywordAutoResponse(content, event);
//                        content = null;
//                    }
//                }

                if (NetUtil.isNewTask) {

                    MassMessageUtil.massMessage(this, NetUtil.text);
                }
                if (MainActivity.isSaveMyWechatInfo) {
                    /**
                     * 获取客服微信信息
                     */
                    Log.e(TAG, "获取微信客服号信息");
                    SaveInfoUtil.getMyselfInfo(this);
                    MainActivity.isSaveMyWechatInfo = false;
                }

                break;


        }
    }


    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    private void fromFriendAddNotification(AccessibilityEvent event) {
        fromNotification = true;
        if (event.getParcelableData() != null && event.getParcelableData() instanceof Notification) {
            notification = (Notification) event.getParcelableData();
            content = notification.tickerText.toString();
            if (!TextUtils.isEmpty(content)) {
//            判断通知栏信息是不是加好友的信息
                isAddFriend = content.contains("请求添加你为朋友");
            } else {
                Log.e(RobotService.TAG, "通知栏监听到其他应用的信息");
            }
            if (isAddFriend) {

//获取好友昵称
                String nickName = content.substring(0, content.indexOf("请求添加你为朋友"));
                Log.e(TAG, "content:" + content);
                Log.e(TAG, "好友昵称:" + nickName);
//                获取好友的备注与标签
//                NetUtil.getRemarksAndTag(nickName, Config.MCH);
//测试
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        NetUtil.getRemarksAndTag("Anson", "25");

                    }
                }).start();
//                NetUtil.getRemarksAndTag("Anson", "25");
//            保存好友昵称
                editor.putString("好友昵称", nickName);
                boolean isSaveNickName = editor.commit();
                if (isSaveNickName) {
                    Log.e(RobotService.TAG, "好友微信昵称保存成功");
                }
//            isAddFriend = content.contains("请求添加你为朋友");
                Log.e(TAG, "isAddFriend:" + isAddFriend);
                contents = content.split(":");

                Log.e(TAG, "contents[contents.length-1]:" + contents[contents.length - 1]);

                PendingIntent pendingIntent = notification.contentIntent;
                try {

                    pendingIntent.send();


                } catch (PendingIntent.CanceledException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e(TAG, "通知栏监听到的消息不是添加好友的消息");
            }

        }

    }


    @Override
    public void onInterrupt() {

    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public void keywordAutoResponse(String content, AccessibilityEvent event) {
        /**功能：关键字自动回复
         * content:消息内容
         * 自动回复
         */
        String[] cArray = content.split(":");
        if (cArray.length > 0) {
            //        Log.i(TAG, "聊天内容数组：" + Arrays.toString(cArray));
            contentLast = cArray[cArray.length - 1];


//            if (contentLast.trim().equals("转发朋友圈")) {
//                //             自动回复关键字 2 的内容
//                Log.i(TAG, "自动转发朋友圈");
//                CircleFriendsUtil.autoSendCircleFriendsUtil(this, "xxx");
//
//
//            } else {
////                不是关键字的回复
////                Log.i(TAG, " 不是关键字的回复");
////查找自动回复
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Log.e(TAG, "查询回复内容");
//                        response = NetUtil.getResponse(contentLast.trim());
//                    }
//                }).start();
//
//                try {
//                    Thread.sleep(500);
//                    Log.i(TAG, "是否查找到控件alm：" + String.valueOf(WechatUtils.findViewId(this, BaseLayoutId + "alm")));
////                    根据id查找控件，并将内容粘贴上去
////                    WechatUtils.findViewByIdAndPasteContent(this, BaseLayoutId + "alm", contentLast);
//                    WechatUtils.findViewByIdAndPasteContent(this, BaseLayoutId + "alm", response);
//                    //                    查找发送按钮并点击
//                    WechatUtils.findViewIdAndClick(this, BaseLayoutId + "als");
////                    返回微信
//                    if (WechatUtils.findViewId(this, RobotService.BaseLayoutId + "jv")) {
//                        WechatUtils.findViewIdAndClick(this, RobotService.BaseLayoutId + "jv");
//                    } else {
//                        Thread.sleep(500);
//                        WechatUtils.findViewIdAndClick(this, RobotService.BaseLayoutId + "jv");
//
//                    }
//
//                    BackRobot.backHome();
//
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }

        }
    }


}
