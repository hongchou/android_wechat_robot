package com.wxrobot.bdt.wxrobot.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.wxrobot.bdt.wxrobot.MainActivity;

import java.util.Timer;

public class StartTimerTaskService extends Service {
    Timer timer = new Timer();
    MainActivity.TimerTaskUtil task = new MainActivity.TimerTaskUtil();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        Notification notification = new Notification(R.mipmap.ic_launcher,
//                "有通知到来", System.currentTimeMillis());
//        Intent notificationIntent = new Intent(this, MainActivity.class);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
//                notificationIntent, 0);
//        notification.setLatestEventInfo(this, "这是通知的标题", "这是通知的内容",
//                pendingIntent);
//        startForeground(1, notification);
        Log.d(RobotService.TAG, "onCreate() executed");
        timer.schedule(task, 60 * 1000, 60 * 1000);
        Log.i(RobotService.TAG, "定时任务开启。。。。。。。。");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(RobotService.TAG, "onStartCommand() executed");
        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d(RobotService.TAG, "onDestroy() executed");
        if (timer != null) {
            Log.i(RobotService.TAG, "定时任务取消");
            timer.cancel();
            timer = null;
        }
        if (task != null) {
            task.cancel();
            task = null;
        }
    }
}
