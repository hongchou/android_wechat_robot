package com.wxrobot.bdt.wxrobot.utils;

import android.accessibilityservice.AccessibilityService;

import com.wxrobot.bdt.wxrobot.net.NetUtil;
import com.wxrobot.bdt.wxrobot.service.RobotService;

//群发消息
public class MassMessageUtil {
    /**
     * 群发消息
     */
    public static void massMessage(AccessibilityService accessibilityService, String msg) {
        try {
//        打开微信
//        OpenWechatUtil.openWechat();
//        判断页面中是否有返回按钮

            boolean isBack = WechatUtils.findViewId(accessibilityService, RobotService.BaseLayoutId + "k5");
            Thread.sleep(500);
            if (isBack) {
                //        进入微信主页面
                WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "k5");
            }
//        //        进入微信主页面
//        WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "k5");
//        搜索
            if (WechatUtils.findViewId(accessibilityService, RobotService.BaseLayoutId + "ij")) {
                WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "ij");
                Thread.sleep(500);
            }
//            WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "ij");
//            Thread.sleep(500);
//            输入法覆盖微信界面,直接回到home界面
            if (!WechatUtils.findViewId(accessibilityService, RobotService.BaseLayoutId + "ka")) {
                BackRobot.backHome();
            }
//        粘贴搜索内容
            WechatUtils.findViewByIdAndPasteContent(accessibilityService, RobotService.BaseLayoutId + "ka", "群发助手");

            Thread.sleep(1000);

//        点击搜索结果(群发助手)
//            WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "pm");
//            WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "pn");
            WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "pp");
            Thread.sleep(500);
            WechatUtils.findTextAndClick(accessibilityService, "新建群发");
            Thread.sleep(500);
//        全选
            WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "d2c");
            Thread.sleep(500);
//        下一步
            WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "jq");
            Thread.sleep(500);
//        输入消息
            WechatUtils.findViewByIdAndPasteContent(accessibilityService, RobotService.BaseLayoutId + "alm", msg);
//            WechatUtils.findViewByIdAndPasteContent(accessibilityService, RobotService.BaseLayoutId + "alm", "happy new year!!!");
            Thread.sleep(500);
//        发送
            if (WechatUtils.findViewId(accessibilityService, RobotService.BaseLayoutId + "als")) {
//
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        NetUtil.updateTask(NetUtil.task_id);
                    }
                }).start();
            }
            WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "als");

            Thread.sleep(3000);
            BackRobot.backHome();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
