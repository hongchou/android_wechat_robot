package com.wxrobot.bdt.wxrobot.net;

import android.text.TextUtils;
import android.util.Log;

import com.wxrobot.bdt.wxrobot.bean.Config;
import com.wxrobot.bdt.wxrobot.service.RobotService;
import com.wxrobot.bdt.wxrobot.utils.OpenWechatUtil;
import com.wxrobot.bdt.wxrobot.utils.StreamTools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NetUtil {
    private static String rps;
    public static int flag = 0;
    public static final String taskUrl = "http://192.168.1.123:81/api/robot/task";
    public static boolean isNewTask = false;
    public static String text;
    public static String task_id;
    public static String status;
    //1.创建OkHttpClient对象
    public static OkHttpClient okHttpClient = new OkHttpClient();
    //2.创建Request对象，设置一个url地址（百度地址）,设置请求方式。
//    public static Request request = new Request.Builder().url("http://www.baidu.com").method("GET", null).build();
    //3.创建一个call对象,参数就是Request请求对象
//    public static okhttp3.Call call = okHttpClient.newCall(request);

    public static void asynchronousGet(String url) {
        /**
         * 异步GET请求
         */
//        //1.创建OkHttpClient对象
//        OkHttpClient okHttpClient = new OkHttpClient();
//        //2.创建Request对象，设置一个url地址（百度地址）,设置请求方式。
//        Request request = new Request.Builder().url("http://www.baidu.com").method("GET", null).build();
//        for (int i = 0; i < args.length; i++) {
////            out.println(args[i]);
//            String parameter=args[i];
//        }
        Request request = new Request.Builder().url(url).method("GET", null).build();
//        //3.创建一个call对象,参数就是Request请求对象
        okhttp3.Call call = okHttpClient.newCall(request);
        //4.请求加入调度，重写回调方法
        call.enqueue(new Callback() {
            //请求失败执行的方法
            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
                Log.e(RobotService.TAG, "异步get请求失败！");
            }

            //请求成功执行的方法
            @Override
            public void onResponse(okhttp3.Call call, Response response) throws IOException {

            }


        });
    }

//    public static void synchronizeGet(String url, String... args) {
//        /**
//         * 同步GET请求
//         */
//        //1.创建OkHttpClient对象
////    OkHttpClient okHttpClient = new OkHttpClient();
////    //2.创建Request对象，设置一个url地址（百度地址）,设置请求方式。
//        Request request = new Request.Builder().url("http://www.baidu.com").method("GET", null).build();
////    //3.创建一个call对象,参数就是Request请求对象
//        final Call call = okHttpClient.newCall(request);
//        //4.同步调用会阻塞主线程,这边在子线程进行
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    //同步调用,返回Response,会抛出IO异常
//                    Response response = call.execute();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }).start();
//
//    }

    public static void asynchronousPost() {
        /**
         * 异步Post请求
         */
        //2.通过new FormBody()调用build方法,创建一个RequestBody,可以用add添加键值对
        RequestBody requestBody = new FormBody.Builder().add("name", "zhangqilu").add("age", "25").build();
        //3.创建Request对象，设置URL地址，将RequestBody作为post方法的参数传入
        Request request = new Request.Builder().url("url").post(requestBody).build();
        //4.创建一个call对象,参数就是Request请求对象
        Call call = okHttpClient.newCall(request);
        //5.请求加入调度,重写回调方法
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
            }
        });

    }

    public static String getTask() {
        String Parameters = "";
        String rvalue = "";
        try {
            //声明URL
            URL url = new URL(taskUrl);
            //打开连接
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            //设置连接方式
            conn.setRequestMethod("POST");
            //设置是否输入参数
            conn.setDoOutput(true);
            //输入参数
            conn.getOutputStream().write(Parameters.getBytes());
            //获取返回值
            InputStream inStream = conn.getInputStream();
            //流转化为字符串
            rvalue = StreamTools.streamToStr(inStream);
            Log.e(RobotService.TAG, "rvalue:" + rvalue);
            try {
                JSONObject jsonObject = new JSONObject(rvalue);
                String data = jsonObject.optString("data");
                JSONObject jsonObject2 = new JSONObject(data);
                String data2 = jsonObject2.optString("data");
                JSONArray jsonArray = new JSONArray(data2);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject job = (JSONObject) jsonArray.get(i);


                    text = job.optString("text");
                    status = job.optString("status");
                    task_id = job.optString("id");
//                        String task_type = job.optString("task_type");

                    Log.e(RobotService.TAG, "text:" + text + "\t" + "status:" + status + "\t" + "task_id:" + task_id);
                    if (!TextUtils.isEmpty(text)) {
                        isNewTask = true;
                        OpenWechatUtil.openWechat();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return rvalue;
    }

    public static void getRemarksAndTag(String nickname, String mch) {
        /**
         * 获取备注和标签
         */


        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        JSONObject json = new JSONObject();

        try {
            json.put("nickname", nickname);
            json.put("mch", mch);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //申明给服务端传递一个json串
        //创建一个OkHttpClient对象
        OkHttpClient okHttpClient = new OkHttpClient();
        //创建一个RequestBody(参数1：数据类型 参数2传递的json串)
        //json为String类型的json数据
        RequestBody requestBody = RequestBody.create(JSON, String.valueOf(json));
        //创建一个请求对象
        String format = String.format("http://192.168.1.123:81/api/robot/add/friend/test");

        Request request = new Request.Builder()
                .url(format)
                .post(requestBody)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(RobotService.TAG, "获取好友备注和标签失败.........");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
//                boolean
                String string = response.body().string();
                Log.e(RobotService.TAG, "获取好友备注和标签成功.........");
                Log.e(RobotService.TAG, "获取好友备注和标签结果：" + string);
                try {
                    JSONObject jsonObject = new JSONObject(string);
                    int status = jsonObject.getInt("status");
                    if (status == 200) {
                        Log.e(RobotService.TAG, "获取好友备注和标签请求成功！");
//                        解析数据
                        String data = jsonObject.getString("data");
                        Log.e(RobotService.TAG, "getRemarksAndTag data：" + data);
                        JSONObject jsonData = new JSONObject(data);
                        String tag = jsonData.getString("tag");
                        Log.e(RobotService.TAG, "getRemarksAndTag tag：" + tag);
                        Config.FRIEND_TAG = tag;
                        String result = jsonData.getString("result");
                        Log.e(RobotService.TAG, "getRemarksAndTag result：" + result);
                        Config.REMARK = result;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    public static void getMch(String robotWxId) {
        String url = "http://47.106.84.115:8051/awxrobot/api/query/weixin/by/account/?wx_account=" + robotWxId;
        Request request = new Request.Builder().url(url).method("GET", null).build();
//        //3.创建一个call对象,参数就是Request请求对象
        okhttp3.Call call = okHttpClient.newCall(request);
        //4.请求加入调度，重写回调方法
        call.enqueue(new Callback() {
            //请求失败执行的方法
            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
                Log.e(RobotService.TAG, "异步get请求失败！");
            }

            //请求成功执行的方法
            @Override
            public void onResponse(okhttp3.Call call, Response response) throws IOException {
                Log.e(RobotService.TAG, "异步get请求成功！");
                String result = response.body().string();
                Log.e(RobotService.TAG, "getMch result:" + result);
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    String data = jsonObject.getString("data");
                    data = data.substring(1, data.length() - 1);
                    Log.e(RobotService.TAG, "getMch data:" + data);
                    JSONObject jsonData = new JSONObject(data);
                    String mch_id = jsonData.getString("mch_id");
                    Log.e(RobotService.TAG, "getMch mch_id:" + mch_id);
                    Config.MCH = mch_id;
                    String gzh_name = jsonData.getString("gzh_name");
                    Log.e(RobotService.TAG, "getMch gzh_name:" + gzh_name);
                    Config.GZH_NAME = gzh_name;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        });

    }
//    public static String getResponse(String msg) {
//
//        /***
//         * 自动回复
//         */
//
//        String baseUrl = "http://192.168.1.122:8051/awxrobot/api/query/all/auto_response/?msg=";
//        Log.e(RobotService.TAG, "url:" + baseUrl + msg);
//        final OkHttpClient client = new OkHttpClient();
//
////        同步 GET
//        final Request request = new Request.Builder().url(baseUrl + msg).build();
//
//        try {
//            Response response = client.newCall(request).execute();
//            if (response.isSuccessful()) {
//                String responseStr = response.body().string();
//                Log.e(RobotService.TAG, "Get 成功");
//                Log.e(RobotService.TAG, "responseStr:" + responseStr);
////                解析json
//                JSONObject object = new JSONObject(responseStr);
//                String data = object.optString("data");
//                JSONArray array = new JSONArray(data);
//                if (array.length() > 0) {
//                    JSONObject js = array.getJSONObject(0);
//
//                    rps = js.getString("response");
//
//                    Log.e(RobotService.TAG, "rps:" + rps);
//                } else {
//                    Log.e(RobotService.TAG, "rps is not found");
//                    rps = "[微笑]";
//                }
//
//            } else {
//                Log.e(RobotService.TAG, "自动回复接口请求失败");
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return rps;
//
//    }

    //    public static void updateTask(String task_id, String status) {
    public static void updateTask(String task_id) {
//        task_id = "5";
//        status = "3";
        /***
         * 更新任务
         */
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        JSONObject json = new JSONObject();

        try {
            json.put("task_id", task_id);
//            json.put("task_id", "5");
//            json.put("status", "1");
            json.put("status", "3");
//            json.put("task_id", task_id);
//            json.put("status", status);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //申明给服务端传递一个json串
        //创建一个OkHttpClient对象
        OkHttpClient okHttpClient = new OkHttpClient();
        //创建一个RequestBody(参数1：数据类型 参数2传递的json串)
        //json为String类型的json数据
        RequestBody requestBody = RequestBody.create(JSON, String.valueOf(json));
        //创建一个请求对象
//                        String format = String.format(KeyPath.Path.head + KeyPath.Path.waybillinfosensor, username, key, current_timestamp);
        String format = String.format("http://192.168.1.123:81/api/robot/task/update");

        Request request = new Request.Builder()
                .url(format)
                .post(requestBody)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //DialogUtils.showPopMsgInHandleThread(Release_Fragment.this.getContext(), mHandler, "数据获取失败，请重新尝试！");
                Log.e(RobotService.TAG, "更新任务失败.........");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String string = response.body().string();
                Log.e(RobotService.TAG, "更新任务成功.........");
                Log.e(RobotService.TAG, "任务更新结果：" + string);

            }
        });
    }
}
