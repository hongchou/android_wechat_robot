package com.wxrobot.bdt.wxrobot.bean;

import java.util.List;

public class TaskInfo {

    private List<Data> data;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "TaskInfo{" +
                "data=" + data +
                '}';
    }
}
