package com.wxrobot.bdt.wxrobot.utils;

import android.accessibilityservice.AccessibilityService;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.wxrobot.bdt.wxrobot.service.RobotService;

public class CircleFriendsUtil {
    /**
     * 功能：自动转发朋友圈
     *
     * @param accessibilityService 无障碍服务
     * @param content              朋友圈内容
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void autoSendCircleFriendsUtil(AccessibilityService accessibilityService, String content) {

        try {
//            点击返回

            WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "jv");
            Thread.sleep(500);
            //        找控件，主页点击发现
            WechatUtils.findTextAndClick(accessibilityService, "发现");
            Thread.sleep(500);
//            WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "qr");
            WechatUtils.findTextAndClick(accessibilityService, "朋友圈");
//            Thread.sleep(1000);
            if (WechatUtils.findViewId(accessibilityService, RobotService.BaseLayoutId + "cvz")) {
                //            转发朋友圈
                WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "cvz");
            } else {
                Thread.sleep(500);
                WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "cvz");
            }
//            转发朋友圈
//            WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "cvz");
//            Thread.sleep(500);

            WechatUtils.findTextAndClick(accessibilityService, "更多");
            Thread.sleep(1500);

//            }
            WechatUtils.findTextAndClick(accessibilityService, "分享到朋友圈");
            Thread.sleep(1000);
//            WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "jq");
            boolean jq = WechatUtils.findViewId(accessibilityService, RobotService.BaseLayoutId + "jq");

            if (jq) {
                Log.e(RobotService.TAG, "点击发表按钮 ==================== ");
                WechatUtils.findTextAndClick(accessibilityService, "发表");
                Thread.sleep(1000);
                //            点击物理键home
                BackRobot.backHome();
            }


        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
