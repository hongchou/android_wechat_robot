package com.wxrobot.bdt.wxrobot.utils;

import android.accessibilityservice.AccessibilityService;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;

import com.wxrobot.bdt.wxrobot.bean.Config;
import com.wxrobot.bdt.wxrobot.service.RobotService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AutoAddFriendAcceptUtil {

    public static boolean isGetFriendInfo = false;

    private static String friendWxId;
    private static String friendNickName;
    //    获取要推送的信息的URL地址
    private static final String getDataUrl = "http://47.106.84.115:8051/awxrobot/api/query/weixin/by/account/?wx_account=";
    //    截取前的微信号
    private static String all_wx_id;
    //    截取后的微信号
    private static String wx_id;
    //    要推送的链接信息
    public static String address;
    //    公众号名字
    public static String gzh_name;
    private static String mch_id;
//    public static String pushUrl;
    private static String url_short;
    private static boolean addTag = false;
    private static boolean addRemarks = false;
    //    添加备注
    private static boolean onceAddRemark = true;
//
//    public static String getShort(String originalUrl) {
///**生成短链接
// * */
//        try {
//            String requestUrl = "http://api.t.sina.com.cn/short_url/shorten.json?source=2815391962&url_long=" + URLEncoder.encode(originalUrl, "utf-8");
//            URL url = new URL(requestUrl);
//            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            connection.setRequestMethod("GET");
//            connection.setConnectTimeout(80000);
//            connection.setReadTimeout(8000);
//            InputStream stresm = connection.getInputStream();
//            //对返回的输入流进行读取
//            BufferedReader reader = new BufferedReader(new InputStreamReader(stresm));
//            StringBuilder response = new StringBuilder();
//            String line;
//            while ((line = reader.readLine()) != null) {
//
//                response.append(line);
//            }
//            Log.e(RobotService.TAG, "短链请求返回值：" + response.toString());
//
////            String jsonData = response.toString().substring(1, (response.toString().length() - 1));
//            String jsonData = response.toString().substring(1, response.toString().length() - 1);
//            JSONObject data = new JSONObject(jsonData);
//            url_short = data.getString("url_short");
//
//            Log.e(RobotService.TAG, "url_short：" + url_short);
////                            if (TextUtils.isEmpty(address) && TextUtils.isEmpty(gzh_name) && TextUtils.isEmpty(mch_id)) {
//
//
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (JSONException e) {
//            e.printStackTrace();
//            Log.e(RobotService.TAG, "生成短链接，json解析异常");
//        }
//        if (!TextUtils.isEmpty(url_short)) {
//            Log.e(RobotService.TAG, "短链接生成成功！");
//            return url_short;
//
//        } else {
//
//            Log.e(RobotService.TAG, "短链接生成失败！");
//            return originalUrl;
//        }
//
//
//    }

    /**
     * 功能：自动添加好友并推送消息
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void autoAddFriendAccept(AccessibilityService accessibilityService) {
//        获取微信客服号的微信号
        all_wx_id = SharedPreferencesUtil.sharedPreferences.getString("微信号", "");
        if (!TextUtils.isEmpty(all_wx_id)) {
//截取客服微信号
            wx_id = all_wx_id.substring(4);
            Log.e(RobotService.TAG, "截取后获取的客服微信号：" + wx_id);
            //        获取要推送给好友的URL
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String mUrl = getDataUrl + wx_id;
                        Log.e(RobotService.TAG, "mUrl:" + mUrl);
                        URL url = new URL(mUrl);
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        //自由定制
                        connection.setRequestMethod("GET");
                        connection.setConnectTimeout(80000);
                        connection.setReadTimeout(8000);
//            使用getInputStream获取服务器返回的输入流
                        InputStream stresm = connection.getInputStream();
                        //对返回的输入流进行读取
                        BufferedReader reader = new BufferedReader(new InputStreamReader(stresm));
                        StringBuilder response = new StringBuilder();
                        String line;
                        while ((line = reader.readLine()) != null) {

                            response.append(line);
                        }
                        Log.e(RobotService.TAG, "返回值：" + response.toString());
                        try {

                            JSONObject jsonObject = new JSONObject(response.toString());
                            JSONArray data = new JSONArray(jsonObject.getString("data"));

                            address = data.getJSONObject(0).getString("address");
                            Log.e(RobotService.TAG, "推送链接：" + address);

                            mch_id = data.getJSONObject(0).getString("mch_id");

                            Log.e(RobotService.TAG, "公众号id：" + mch_id);

//                            if (TextUtils.isEmpty(address) && TextUtils.isEmpty(gzh_name) && TextUtils.isEmpty(mch_id)) {
                            if (TextUtils.isEmpty(address) && TextUtils.isEmpty(mch_id)) {
                                Log.e(RobotService.TAG, "没有获取到要推送的信息-----------");

                            } else {
                                Log.e(RobotService.TAG, "获取到要推送的信息");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(RobotService.TAG, "解析数据异常。。。。。。。。。");
                        }

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                }
            }).start();

        }
//        try {

        WechatUtils.findTextAndClick(accessibilityService, "接受");

//        备注输入框ID：e0s
//        addRemarks = WechatUtils.findViewByIdAndPasteContent(accessibilityService, RobotService.BaseLayoutId + "e0s", "备注xxxx");
//        if (WechatUtils.findViewId(accessibilityService, RobotService.BaseLayoutId + "e0s") && TextUtils.isEmpty(Config.REMARK)) {

        if (WechatUtils.findViewId(accessibilityService, RobotService.BaseLayoutId + "e0s") && onceAddRemark) {
            addRemarks = WechatUtils.findViewByIdAndPasteContent(accessibilityService, RobotService.BaseLayoutId + "e0s", Config.REMARK);
//            备注设置完成
            onceAddRemark = false;
        } else {
//            Log.e(RobotService.TAG, "没有找到备注栏控件，或者获取的备注信息为空");
            Log.e(RobotService.TAG, "没有找到备注栏控件");
            try {
//            延时加载控件
                Thread.sleep(500);
                addRemarks = WechatUtils.findViewByIdAndPasteContent(accessibilityService, RobotService.BaseLayoutId + "e0s", Config.REMARK);
                //            备注设置完成
                onceAddRemark = false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
//        标签：e0v
//        if (addRemarks && adding) {
        if (addRemarks) {
//            点击 e0v 进入标签设置页面
            WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "e0v");
            try {
//                给kh控件加载时间
//                Thread.sleep(500);
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();

            }
            boolean findTage = WechatUtils.findViewId(accessibilityService, RobotService.BaseLayoutId + "kh");
            if (findTage) {
                Log.e(RobotService.TAG, "获取标签栏");
                //            设置标签，标签输入框ID：kh
//                addTag = WechatUtils.findViewByIdAndPasteContent(accessibilityService, RobotService.BaseLayoutId + "kh", "标签xxxx");
                addTag = WechatUtils.findViewByIdAndPasteContent(accessibilityService, RobotService.BaseLayoutId + "kh", Config.FRIEND_TAG);
                WechatUtils.findTextAndClick(accessibilityService, "保存");
//                adding = false;
//            返回完成页面，返回ID：kb
//                WechatUtils.findViewIdAndClick(accessibilityService, RobotService.BaseLayoutId + "kb");
            } else {
                Log.e(RobotService.TAG, "没有获取到标签栏");
            }
        }
        if (addTag) {
            WechatUtils.findTextAndClick(accessibilityService, "完成");
        }
/**
 *  此处必须长时间延时
 */
//            Thread.sleep(2000);


//            获取好友信息

//            if (WechatUtils.findViewId(accessibilityService, RobotService.BaseLayoutId + "b2x") && WechatUtils.findViewId(accessibilityService, RobotService.BaseLayoutId + "b35")) {
        if (WechatUtils.findViewId(accessibilityService, RobotService.BaseLayoutId + "b45")) {
//                String wxId = WechatUtils.findTextById(accessibilityService, RobotService.BaseLayoutId + "b35");
            String wxId = WechatUtils.findTextById(accessibilityService, RobotService.BaseLayoutId + "b45");
            Log.e(RobotService.TAG, "微信号：" + wxId);

            if (wxId != null)
                SharedPreferencesUtil.editor.putString("微信号", wxId);

            boolean isCommit = SharedPreferencesUtil.editor.commit();
            if (isCommit) {
                isGetFriendInfo = true;
                Log.e(RobotService.TAG, "好友微信号保存成功！");
//保存好友微信信息

                friendWxId = SharedPreferencesUtil.friendSharedPreferences.getString("微信号", "");
                friendNickName = SharedPreferencesUtil.friendSharedPreferences.getString("好友昵称", "");
                if (!TextUtils.isEmpty(friendWxId) && !TextUtils.isEmpty(friendNickName)) {
                    Log.e(RobotService.TAG, "好友微信信息保存成功！-------------");
//                    组装要推送的链接信息
//                    好友微信号
//                    String friendWxId = SharedPreferencesUtil.friendSharedPreferences.getString("微信号", "").substring(6);
                    friendWxId = friendWxId.substring(6);
//                    当前时间戳
//                    String dateStr = String.valueOf(System.currentTimeMillis());
//                    if (TextUtils.isEmpty(friendWxId)) {
//                        Log.e(RobotService.TAG, "没有获取到好友微信号");
//                    } else {
                    Log.e(RobotService.TAG, "获取到处理后的好友微信号======:" + friendWxId);
//                        String dataUrl = "robot" + "@" + address + "@" + friendWxId + "@" + mch_id + "@" + dateStr;
//                    String bStr = "robot" + "@" + friendWxId + "@" + wx_id + "@" + mch_id + "@" + dateStr;
//                    String pStr = Base64.encodeToString(bStr.getBytes(), Base64.DEFAULT);
//                    Log.e(RobotService.TAG, "加密后的数据：" + pStr);
//                        pushUrl = address + "/?robot=" + pStr;
//                    pushUrl = address + "/index.php?m=&c=Index&a=readHistory&robot=" + pStr;
//                    }
//                    new Thread(new Runnable() {
//                        @Override
//                        public void run() {
//                            pushUrl = getShort(pushUrl);
//
//                        }
//                    }).start();
//                    pushUrl = getShort(pushUrl);

                } else {
                    Log.e(RobotService.TAG, "好友微信信息不完整，不存储");
                }
            }
        }


//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }


    }
}
