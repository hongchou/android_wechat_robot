package com.wxrobot.bdt.wxrobot.utils;

import android.content.Intent;

import com.wxrobot.bdt.wxrobot.MyApplication;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

//打开微信
public class OpenWechatUtil {
    /***
     * 打开微信
     */
    public static void openWechat() {
        Intent intent = new Intent();
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
        intent.setClassName("com.tencent.mm", "com.tencent.mm.ui.LauncherUI");
        MyApplication.getInstance().startActivity(intent);
    }
}
