package com.wxrobot.bdt.wxrobot.utils;

import android.app.Activity;
import android.content.SharedPreferences;

import com.wxrobot.bdt.wxrobot.MyApplication;

public class SharedPreferencesUtil {
    /**
     * 首选项工具类
     */
//    好友微信信息
    public static SharedPreferences friendSharedPreferences = MyApplication.getInstance().getSharedPreferences("friend_wechat", Activity.MODE_PRIVATE);
    public static SharedPreferences.Editor editor = friendSharedPreferences.edit();

//    客服微信信息
    public static SharedPreferences sharedPreferences = MyApplication.getInstance().getSharedPreferences("my_wechat", Activity.MODE_PRIVATE);
    public static SharedPreferences.Editor mEditor = sharedPreferences.edit();
}
