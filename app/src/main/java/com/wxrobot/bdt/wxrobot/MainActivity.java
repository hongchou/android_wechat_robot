package com.wxrobot.bdt.wxrobot;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.wxrobot.bdt.wxrobot.net.NetUtil;
import com.wxrobot.bdt.wxrobot.service.RobotService;
import com.wxrobot.bdt.wxrobot.utils.SharedPreferencesUtil;

import java.util.TimerTask;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button;
    private Context mContext;
    private Button start_timer;
    private Button save_my_info;
    //    private Button bt_test_bugly;
    public static boolean isSaveMyWechatInfo = false;
    private static String temp_nickeName;
    private static String temp_wxid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
//        NetUtil.getMch("xxy1674");
//        NetUtil.getRemarksAndTag("Anson", "25");
        mContext = this;


        button = (Button) findViewById(R.id.button);
//        start_timer = (Button) findViewById(R.id.bt_start_timer);
        save_my_info = (Button) findViewById(R.id.bt_save_my_info);
//        bt_test_bugly = (Button) findViewById(R.id.bt_test_bugly);
//        start_timer.setOnClickListener(this);
        save_my_info.setOnClickListener(this);
//        bt_test_bugly.setOnClickListener(this);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                启动服务

                if (isAccessibilitySettingsOn(mContext)) {
                    RobotService.isSendSuccess = false;
                    openWChart();
                } else {
                    Intent accessibleIntent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                    startActivity(accessibleIntent);
                }
            }
        });

    }

    /**
     * 打开微信界面
     */
    private void openWChart() {

        Intent intent = new Intent();
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
        intent.setClassName("com.tencent.mm", "com.tencent.mm.ui.LauncherUI");
        startActivity(intent);

    }

    /**
     * 判断微信助手是否开启
     *
     * @param context
     * @return
     */
    public boolean isAccessibilitySettingsOn(Context context) {
        int accessibilityEnabled = 0;
        try {
            accessibilityEnabled = Settings.Secure.getInt(context.getContentResolver(),
                    Settings.Secure.ACCESSIBILITY_ENABLED);
        } catch (Settings.SettingNotFoundException e) {
            Log.i("URL", "错误信息为：" + e.getMessage());
        }

        if (accessibilityEnabled == 1) {
            String services = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (services != null) {
                return services.toLowerCase().contains(context.getPackageName().toLowerCase());
            }
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

//            case R.id.bt_start_timer:
//                Intent startIntent = new Intent(this, StartTimerTaskService.class);
//                startService(startIntent);
//                break;
            case R.id.bt_save_my_info:

                //        先读取保存客服号信息的文件
                temp_nickeName = SharedPreferencesUtil.sharedPreferences.getString("微信昵称", "");
                temp_wxid = SharedPreferencesUtil.sharedPreferences.getString("微信号", "");
                if (TextUtils.isEmpty(temp_nickeName) || TextUtils.isEmpty(temp_wxid)) {
                    /**
                     * 判断微信客服号的信息是否存在（不存在）
                     */
                    isSaveMyWechatInfo = true;
                    openWChart();
                } else {
                    Toast.makeText(this, "微信客服号信息已经存在", Toast.LENGTH_SHORT).show();

                }
//                isSaveMyWechatInfo = true;
//                openWChart();
                break;

//            case R.id.bt_test_bugly:
//
//                //                测试bugly
////                CrashReport.testJavaCrash();
//
//
//                break;
        }
    }


    public static class TimerTaskUtil extends TimerTask {

        @Override
        public void run() {
            Log.i(RobotService.TAG, "开始执行执行timer定时任务...");

            NetUtil.getTask();
        }

    }

}
